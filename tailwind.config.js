const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
	theme: {
		fontFamily: {
			'body': ['Akzidenz-Grotesk', ...defaultTheme.fontFamily.sans]
		},
		backgroundColor: theme => ({
			...theme('colors'),
			'brand-primary': '#293894',
			'brand-gray': '#CFD1C7',
		}),
		extend: {
			spacing: {
				'36': '9rem',
				'44': '11rem'
			}
		}
	},
	variants: {},
	plugins: []
}
